---
layout: post
title:  "7 sitios fraudulentos en Costa Rica"
date:   2020-10-10 09:00:00 -0600
author: Just Another Costa Rican
image: sitos-falsos-en-costa-rica.webp
tags: videos phishing
---

Durante 2020 la ciberdelincuencia ha creado múltiples sitios web fraudulentos con la intención engañar a los costarricenses y hacer que entreguen información confidencial de cuentas bancarias como nombres de usuario y contraseñas, lamentablemente muchas personas han caído en el engaño y terminan perdiendo grandes cantidades de dinero.

Cuando alguien es víctima de una de estas estafas, le vacían la cuenta y las aseguradoras no se hacen responsables al respecto pues se han entregado de forma voluntaria los accesos a terceras personas.

Es por eso que para prevenir que más personas caigan en estas tipo de engaños les comparto **7 sitios fraudulentos en Costa Rica** captados en video paso a paso.

> Sin imoprtar a cual de los sitios te hagan entrar el flujo es siempre el mismo

## Flujo del contacto

- Llamada telefónica entrante
- Se hacen pasar por funcionarios estatales o bancarios
- Ofrecen nombres, números y direcciones, para dar legitimidad
- Informan acerca de un evento que debe ser atendido de inmediato
  - Supuestos movimientos de dineros sospechosos
  - Supuestas devoluciones de impuestos de Hacienda
  - Retiros del FCL
  - Depósitos del Bono Proteger
  - Citas para la CCSS
  - *Entre muchas otras excusas*-
- Para hacer el tramite, sacar la cita o solicitar la firma digital le hacen entrar a un sitio fraudulento-
- En este sitio solicitan nombres de usuario, contraseñas, códigos de seguridad, números de tarjeta, etc.-
- Suelen mencionar que es un sitio seguro y que no brinde datos por teléfono, con el objetivo de que la víctima acceda-
- Una vez la víctima escribe sus datos los ciberdelincuentes inmediatamente ingresan a los sitios de banca en línea-
- Mantienen a la víctima un rato más al teléfono para tener una ventana de tiempo donde vaciar la cuenta-
- Termina la llamada sin mayor explicación


## Video 1 - Sitio Falso de Firma digital A

En el siguiente video se aprecia un sitio en el que los atacantes hacen entrar a las víctimas con la promesa de obtener una firma digital para concluir un supuesto trámite.

> En Costa Rica la obtención de una firma digital solo puede darse en el ambiente controlado de una entidad bancaria de forma precencial **nunca por internet**

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21107&authkey=AC8H0dir5-oLTnU" type="video/mp4">
  Your browser does not support the video tag.
</video>

<hr>

## Video 2 - Sitio Falso de Banco Central A

En este video se puede ver un sitio web que utiliza el nombre del Banco Central de Costa Rica para confundir a sus víctimas y solicitar datos confidenciales.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21109&authkey=AH0JXAvicfprF_I" type="video/mp4">
  Your browser does not support the video tag.
</video>

<hr>

## Video 3 - Sitio Falso de Firma digital B

Este sitio también muestra el nombre de Firma digital para llevar a cabo su estafa.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21110&authkey=ABPFzTNYbqCslzw" type="video/mp4">
  Your browser does not support the video tag.
</video>

<hr>

## Video 4 - Sitio Falso de Firma digital C

Otra vación de la estafa a utilizando el nombre de Firma digital.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21111&authkey=APmjwA7cAv22g0I" type="video/mp4">
  Your browser does not support the video tag.
</video>

<hr>

## Video 5 - Sitio Falso de Firma digital D

Una quinta variación.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21112&authkey=AA8mJm7WIZhfuWA" type="video/mp4">
  Your browser does not support the video tag.
</video>

<hr>

## Video 6 - Sitio Falso de Firma digital E

Esta es la versión más reciente de la misma estafa.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21113&authkey=ACg7NrgPuR9QjvU" type="video/mp4">
  Your browser does not support the video tag.
</video>

<hr>


## Video 7 - Sitio Falso de Ministerio de salud A

En este caso observamos un sitio que dice ser del Ministerio de Salud pero se comporta de la misma forma que los sitios de firma digital que hemos visto.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21117&authkey=ABfptbu6R8qmVWI" type="video/mp4">
  Your browser does not support the video tag.
</video>