---
layout: post
title:  "Lista de dominios fraudulentos"
date:   2020-10-12 09:00:00 -0600
author: Just Another Costa Rican
image: lista-de-dominios-fraudulentos.webp
tags: phishing dominios
---

Como efecto de la situación de confinamiento así como el complejo panorama económico que se vive en Costa Rica la cantidad de sitios fraudulentos que se registran cotidianamente ha incrementado. A continuación se presenta una extensa lista de dominios fraudulentos utilizados para estafar en Costa Rica.

> Es importante aclarar que no se trata de una lista completa, sino solo algunos de los dominios encontrados.

Dado que la lista de nombres es muy larga, resulta útil comprender que en la mayoría de los sitios aquí listado se repiten innumerables páginas idénticas  las cuales han sido captadas en video y compartidas en otro post, el cual puedes ver <a href="https://just-another-costa-rican.gitlab.io/2020/10/10/sitos-falsos-en-costa-rica/" target="_blank">dando clic aquí</a>

## Nube de palabra

Las palabras clave más utilizadas por los delincuentes para los dominios que compran se encuentran en la siguiente nube de palabras
![numbe-de-palabra-clave-sitios-fraudulentos](/images/numbe-de-palabra-clave-sitios-fraudulentos.webp)

Las 8 palabras que más se repiten en estos dominios son

1. **digital**, *60 veces*
1. **bccr**, *44 veces*
1. **fcl**, *43 veces*
1. **cr**, *31 veces*
1. **firma**, *29 veces*
1. **supen**, *26 veces*
1. **soporte**, *23 veces*
1. **certificado**, *13 veces*

## Lista de sitios

Esta lista contine dominios comprados de Marzo a Octubre 2020. 

La mayoría de estos sitios han sido dados de baja, aún así **no se recomienda visitarlos** sin el acompañamiento de un experto que tome todas las medidas de seguridad.

| Dominio                             | Fecha de compra |
|-------------------------------------|-----------------|
| retirarfclsupen\.com                | 2020\-10\-10    |
| supenfclcr\.com                     | 2020\-10\-10    |
| firmadordigital\.com                | 2020\-10\-10    |
| firmadordigitalgo\.com              | 2020\-10\-10    |
| cita\-bccr\.com                     | 2020\-10\-10    |
| citaprevia\-cr\.com                 | 2020\-10\-08    |
| citapreviacrgo\.com                 | 2020\-10\-07    |
| asistircita\.com                    | 2020\-10\-02    |
| firmadorcertificado\.com            | 2020\-10\-02    |
| fclenlineago\.com                   | 2020\-10\-02    |
| citapersonal2020cr\.com             | 2020\-10\-02    |
| previacitacr\.online                | 2020\-10\-02    |
| registrarcita\.com                  | 2020\-10\-01    |
| citas\-previacr\.com                | 2020\-10\-01    |
| reservarcitaenlinea\.com            | 2020\-09\-30    |
| supen\-lineafcl\.com                | 2020\-09\-29    |
| citaplataforma\.com                 | 2020\-09\-26    |
| retiresufcl\.com                    | 2020\-09\-24    |
| asistenciadigitalcrgo\.com          | 2020\-09\-24    |
| certificafirma\.com                 | 2020\-09\-23    |
| sistema\-digitalgocr\.com           | 2020\-09\-22    |
| soportecr\-digital\.com             | 2020\-09\-22    |
| digitalsupen\.com                   | 2020\-09\-22    |
| bccr\.online                        | 2020\-09\-21    |
| citas\-bancocentralcr\.com          | 2020\-09\-21    |
| citas\-bancocentralcr\.com          | 2020\-09\-21    |
| tramitescertificadodigital\.online  | 2020\-09\-21    |
| soporte\-firmadigital\.com          | 2020\-09\-19    |
| supen\-crgo\.com                    | 2020\-09\-19    |
| supen\-digitalcrgo\.com             | 2020\-09\-19    |
| fcldigital\-crgo\.com               | 2020\-09\-19    |
| asociarfirma\.com                   | 2020\-09\-19    |
| cr\-citapersonal\.com               | 2020\-09\-18    |
| cr\-citaspersonal\.com              | 2020\-09\-18    |
| activarfcl\.com                     | 2020\-09\-17    |
| firmadorbccrgo\.com                 | 2020\-09\-17    |
| sistemabccrfd\.com                  | 2020\-09\-17    |
| fcldigitalsupen\.com                | 2020\-09\-17    |
| activar\-firmadigital\.com          | 2020\-09\-15    |
| activarfirmadigital\.com            | 2020\-09\-15    |
| solicitudfcl\.com                   | 2020\-09\-15    |
| certficadofirmadigital\.com         | 2020\-09\-15    |
| soporte\-digitalcr\.net             | 2020\-09\-15    |
| activar\-firma\.com                 | 2020\-09\-14    |
| retirar\-fcl\.com                   | 2020\-09\-13    |
| solicitudenlineacr\.com             | 2020\-09\-13    |
| citapersonal2020\.com               | 2020\-09\-13    |
| activarfirmadorgo\-cr\.com          | 2020\-09\-12    |
| protocolovirtualgo\-cr\.com         | 2020\-09\-12    |
| acreditarfirma\.com                 | 2020\-09\-11    |
| afiliafirmacr\.com                  | 2020\-09\-11    |
| solicitafcl\.com                    | 2020\-09\-11    |
| servicioactivadorfd\.com            | 2020\-09\-10    |
| citagenerada\.com                   | 2020\-09\-10    |
| fclsupencr\.com                     | 2020\-09\-09    |
| firmafd\.com                        | 2020\-09\-09    |
| firmadorfd\.com                     | 2020\-09\-09    |
| firmaactivacr\.com                  | 2020\-09\-09    |
| fondosfclgo\.com                    | 2020\-09\-08    |
| retiro\-fcl\.com                    | 2020\-09\-08    |
| aplicarfirma\.com                   | 2020\-09\-08    |
| solicita\-firma\.com                | 2020\-09\-08    |
| generar\-firma\.com                 | 2020\-09\-04    |
| genera\-fcl\.com                    | 2020\-09\-04    |
| soporte\-digitalcr\.com             | 2020\-09\-04    |
| generarfcl\.com                     | 2020\-09\-03    |
| generarfirma\.com                   | 2020\-09\-03    |
| soportecertificadodigitalcr\.com    | 2020\-09\-03    |
| firmascrgo\.com                     | 2020\-09\-02    |
| activarfirmabccr\.com               | 2020\-09\-02    |
| activarfirma\.com                   | 2020\-09\-02    |
| pagosenlinea2020\.com               | 2020\-09\-02    |
| fondosupen\.com                     | 2020\-09\-01    |
| retiro\-supen\.com                  | 2020\-09\-01    |
| firmavirtualcr\.com                 | 2020\-09\-01    |
| huella\-digitalcr\.com              | 2020\-08\-31    |
| soportecertificadodigital\-cr\.com  | 2020\-08\-28    |
| formatos\-digitales\.com            | 2020\-08\-28    |
| retirofondofcl\.com                 | 2020\-08\-26    |
| pagos\-enlinea\.co                  | 2020\-08\-26    |
| firmadigital3\.com                  | 2020\-08\-25    |
| firmadigital1\.com                  | 2020\-08\-25    |
| firmadigital2\.com                  | 2020\-08\-24    |
| pago\-enlinea\.com                  | 2020\-08\-24    |
| servicioactivadorfd\.com            | 2020\-08\-23    |
| certificadorvirtualcr\.com          | 2020\-08\-21    |
| soportecertificadordigital\-cr\.com | 2020\-08\-21    |
| gobccr\.com                         | 2020\-08\-19    |
| certificadovirtual\-cr\.com         | 2020\-08\-19    |
| fcl\-digitalcrgo\.com               | 2020\-08\-19    |
| bccr\-certificado\.com              | 2020\-08\-18    |
| citas\-bccr\.com                    | 2020\-08\-18    |
| firmago\-bccr\.com                  | 2020\-08\-18    |
| supenretiros\.com                   | 2020\-08\-18    |
| supencr\-fcl\.com                   | 2020\-08\-17    |
| certificadovirtualcr\.com           | 2020\-08\-17    |
| firmacr\-bccr\.com                  | 2020\-08\-17    |
| citabccr\.com                       | 2020\-08\-17    |
| certificadocr\.online               | 2020\-08\-15    |
| soportevirtualcr\.com               | 2020\-08\-15    |
| digital\-bccr\.com                  | 2020\-08\-14    |
| certificadocrfirma\.com             | 2020\-08\-14    |
| fcldigitalcrgo\.com                 | 2020\-08\-14    |
| bccrfirmacr\.com                    | 2020\-08\-12    |
| bccrfirmago\.com                    | 2020\-08\-12    |
| citaspreviacr\.com                  | 2020\-08\-12    |
| citasprevia2020\.com                | 2020\-08\-12    |
| fcldigital2020\.com                 | 2020\-08\-12    |
| bccrcita\.com                       | 2020\-08\-11    |
| bccrdigitalcr\.com                  | 2020\-08\-11    |
| firmadigitalbccr\.info              | 2020\-08\-11    |
| fcldigital\.com                     | 2020\-08\-11    |
| citasbccr\.com                      | 2020\-08\-10    |
| firmagobccr\.com                    | 2020\-08\-10    |
| fclsupengocr\.com                   | 2020\-08\-07    |
| bccrgocr\.com                       | 2020\-08\-07    |
| citabccr2020\.com                   | 2020\-08\-07    |
| citaprevia\-bccr\.com               | 2020\-08\-07    |
| fclsupen\-cr\.com                   | 2020\-08\-06    |
| soportecitabndf\.com                | 2020\-08\-06    |
| asistenciabccr\.com                 | 2020\-08\-05    |
| supen\-fcl\.com                     | 2020\-08\-05    |
| digitalbccr\.com                    | 2020\-08\-05    |
| firmadigitalng\.com                 | 2020\-08\-05    |
| certificadodigitalcr\.org           | 2020\-08\-05    |
| crsupenfcl\.com                     | 2020\-08\-04    |
| bccr\-digital\.com                  | 2020\-08\-04    |
| bccrfirmadigital\.com               | 2020\-08\-04    |
| sohaciendacr\.digita                | 2020\-08\-04    |
| certificarcita2020\.com             | 2020\-08\-04    |
| sohaciendacr\.digital               | 2020\-08\-04    |
| fcldigitalcr\.com                   | 2020\-08\-03    |
| centralbccr\.com                    | 2020\-08\-02    |
| firma\-bccrdigital\.com             | 2020\-08\-02    |
| soportedigital\-cr\.com             | 2020\-08\-01    |
| soportefirmadigitalcr\.com          | 2020\-07\-31    |
| firmadigitallcr\.online             | 2020\-07\-31    |
| registrartufirmacr\.com             | 2020\-07\-30    |
| certificaciobccr\.com               | 2020\-07\-29    |
| certificacionbccr\.com              | 2020\-07\-29    |
| certificado\-bccr\.com              | 2020\-07\-29    |
| certificadofcl\.com                 | 2020\-07\-29    |
| supend\.com                         | 2020\-07\-27    |
| crdigitalfirma\.com                 | 2020\-07\-27    |
| bccrcostarica\.com                  | 2020\-07\-25    |
| bccr\-firmacr\.com                  | 2020\-07\-24    |
| bccr\-soporte\.com                  | 2020\-07\-24    |
| firmabcr\.online                    | 2020\-07\-24    |
| wwwbancopopularenlinea\.com         | 2020\-07\-24    |
| supengocr\.com                      | 2020\-07\-24    |
| supensd\.com                        | 2020\-07\-23    |
| bccrfirmasfd\.com                   | 2020\-07\-23    |
| centralbccr\.com                    | 2020\-07\-23    |
| transferencias\-enlinea\.com        | 2020\-07\-23    |
| soporteticr\.com                    | 2020\-07\-23    |
| activafirma\.co                     | 2020\-07\-22    |
| bccrsistema\.com                    | 2020\-07\-21    |
| asistenciadigitalcr\.com            | 2020\-07\-21    |
| supenfcl\.com                       | 2020\-07\-21    |
| mi\-firma\.com                      | 2020\-07\-21    |
| bcfirmadigital\.com                 | 2020\-07\-20    |
| supenvirtual\.com                   | 2020\-07\-19    |
| bccrdigitalfd\.com                  | 2020\-07\-19    |
| bccrc\.org                          | 2020\-07\-19    |
| fclsupengo\.com                     | 2020\-07\-17    |
| bccrdigital\.com                    | 2020\-07\-17    |
| bccrsistemadigital\.com             | 2020\-07\-17    |
| asistenciatributaria\.com           | 2020\-07\-17    |
| bccrfirmacrgo\.com                  | 2020\-07\-16    |
| bccrgo\.com                         | 2020\-07\-16    |
| supencr\.com                        | 2020\-07\-15    |
| supenfcldigital\.com                | 2020\-07\-15    |
| bccrfirmadorcr\.com                 | 2020\-07\-15    |
| firmacrdigital2020\.com             | 2020\-07\-14    |
| supencostarica\.com                 | 2020\-07\-13    |
| firmabccr\.com                      | 2020\-07\-13    |
| soportedefd\.com                    | 2020\-07\-11    |
| supencrgo\.com                      | 2020\-07\-10    |
| firmacr\-digital\.com               | 2020\-07\-10    |
| bccrfirmador\.com                   | 2020\-07\-10    |
| firmadigitalsdcr\.com               | 2020\-07\-09    |
| supendigital\.com                   | 2020\-07\-09    |
| firma\-digitalcr\.com               | 2020\-07\-09    |
| supenfclgocr\.com                   | 2020\-07\-08    |
| soportefimacrgo\.com                | 2020\-07\-08    |
| fclsupen\.com                       | 2020\-07\-07    |
| soportego\.com                      | 2020\-07\-07    |
| soportecrdigital\.com               | 2020\-07\-07    |
| soportefirma\.com                   | 2020\-07\-06    |
| firmacrdigital\.com                 | 2020\-07\-05    |
| fclenlinea\.com                     | 2020\-07\-04    |
| crretirosfcl\.com                   | 2020\-07\-03    |
| firmadigitalsd\.com                 | 2020\-07\-03    |
| soportefdcr\.com                    | 2020\-07\-02    |
| solicitarcita2020\.com              | 2020\-07\-02    |
| fcldigitales\.com                   | 2020\-07\-01    |
| soportesfd\.com                     | 2020\-07\-01    |
| respaldosmdh\.center                | 2020\-07\-01    |
| fclretiros\.com                     | 2020\-06\-30    |
| solicitarcitaprevia\.com            | 2020\-06\-27    |
| respaldoenlinea\.com                | 2020\-06\-26    |
| fcl\-digital\.com                   | 2020\-06\-23    |
| fcl\-digitalcr\.com                 | 2020\-06\-23    |
| fondo\-fcl\.com                     | 2020\-06\-23    |
| soporte\-fd\.com                    | 2020\-06\-23    |
| respaldodigitalcr\.com              | 2020\-06\-20    |
| fclfondos\.com                      | 2020\-06\-20    |
| soportebccr\.com                    | 2020\-06\-20    |
| soportedigitalcrgo\.com             | 2020\-06\-20    |
| certificacionfd\.com                | 2020\-06\-18    |
| fclfondo\.com                       | 2020\-06\-18    |
| fondofcl\.com                       | 2020\-06\-18    |
| soportefd\.com                      | 2020\-06\-17    |
| fclturetiro\.com                    | 2020\-06\-17    |
| retiratufcl\.com                    | 2020\-06\-15    |
| certificadofd\.com                  | 2020\-06\-12    |
| firmadigitalmhcrplm\.online         | 2020\-05\-29    |
| soportedigitalcr\.co                | 2020\-05\-28    |
| firmacostarica\.digital             | 2020\-05\-28    |
| accesofirmadigitalmhcr\.com         | 2020\-05\-28    |
| firmadigitalmhcrsiec\.online        | 2020\-05\-27    |
| firmadigitalcrgo\.com               | 2020\-05\-07    |
| firmadigitalgocr\.com               | 2020\-05\-07    |
| bccrfclretiro\.com                  | 2020\-05\-04    |
| fclretiro\.com                      | 2020\-05\-04    |
| firmadigitalgocrl\.com              | 2020\-05\-04    |
| firmadigitalgocrs\.com              | 2020\-05\-04    |
| firmadigitalgocrd\.com              | 2020\-05\-02    |
| retirofclbccr\.com                  | 2020\-05\-02    |
