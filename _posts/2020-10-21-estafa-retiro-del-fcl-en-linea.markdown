---
layout: post
title:  "Estafa retiro del FCL en linea"
date:   2020-10-21 22:00:00 -0600
author: Just Another Costa Rican
image: estafa-retiro-fcl-en-linea.webp
tags: phishing fcl supen video
---

> Buenas tardes señor, le habla el ejecutivo Joaquín Chavez Sandí de la plataforma corporativa SUPEN, Superintendencia Nacional de pensiones; ¿Como me le va el día de hoy?, el motivo de mi llamada es para comunicarle que ya se encuentra disponible el retiro de su **FCL**, Fondo de Capitalización Laboral, te explico de que se trata. El Gobierno de la república junto a su gabinete decretó y ordenó que toda persona costarricense cedula física o jurídica pueda optar por hacer un retiro anticipado de dicho fondo debido a la crisis económica por la que está pasado el país.

Este es uno de los discursos con los que cotidianamente un grupo criminal organizado llama a un sin número de costarricenses con el objetivo de engañarles para que ingresen a un sitio malicioso creado por ellos mismos sobre el cual tienen control absoluto y donde les solicitarán que ingresen información en un **falso inicio de sesión** de cuenta bancaria, datos que utilizarán inmediatamente para realizar un robo, dejando las cuentas en CERO.

Los que se muestra a continuación es una grabación de uno de los sitios que estos delincuentes utilizan para robar información de acceso a cuentas bancarias.

<video width="100%" controls>
  <source src="https://onedrive.live.com/download?cid=6216F852C4A8FE6C&resid=6216F852C4A8FE6C%21120&authkey=ACZZ85mcDXBLzCM" type="video/mp4">
  Your browser does not support the video tag.
</video>
